package com.example.testnavigationfragment.utils

import java.util.*

object VideoIdsProvider {
    private val videoIds = arrayOf("6JYIGclVQdw", "LvetJ9U_tVY", "S0Q4gqBUs7c", "zOa-rSM4nms")
    private val liveVideoIds = arrayOf("hHW1oY26kxQ")
    private val random = Random()

    val nextVideoId: String
        get() = videoIds[random.nextInt(videoIds.size)]

    val nextLiveVideoId: String
        get() = liveVideoIds[random.nextInt(liveVideoIds.size)]
}