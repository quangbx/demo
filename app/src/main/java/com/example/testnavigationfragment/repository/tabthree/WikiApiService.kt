package com.example.testnavigationfragment.repository.tabthree

import com.example.testnavigationfragment.utils.Model
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface WikiApiService {

    @GET("api.php")
    fun hitCountCheck(
        @Query("action") action: String,
        @Query("format") format: String,
        @Query("list") list: String,
        @Query("srsearch") srSearch: String
    ):
            Observable<Model.Result>
}