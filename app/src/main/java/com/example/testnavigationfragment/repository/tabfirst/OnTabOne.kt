package com.example.testnavigationfragment.repository.tabfirst

interface OnTabOne {
    fun onItemChildClick(text: String)
}