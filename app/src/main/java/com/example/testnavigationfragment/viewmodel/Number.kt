package com.example.testnavigationfragment.viewmodel

import androidx.databinding.Bindable
import androidx.databinding.Observable
import androidx.databinding.PropertyChangeRegistry
import com.example.testnavigationfragment.BR


class Number : Observable {
    private val registry = PropertyChangeRegistry()

    @get:Bindable
    var firstName: String = ""
        set(value) {
            field = value
            registry.notifyChange(this, BR.firstName)
        }

    @get:Bindable
    var lastName: String = ""
        set(value) {
            field = value
            registry.notifyChange(this, BR.lastName)
        }

    override fun removeOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
        registry.remove(callback)
    }

    override fun addOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
        registry.add(callback)
    }
}