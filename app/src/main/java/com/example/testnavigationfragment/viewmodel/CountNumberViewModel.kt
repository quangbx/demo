package com.example.testnavigationfragment.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlin.random.Random

class CountNumberViewModel : ViewModel() {
    val scoreTeamA = MutableLiveData<Int>()
    val scoreTeamB = MutableLiveData<Int>()
    var number: Number

    fun increaseScoreTeamA(score: Int) {
        scoreTeamA.postValue(scoreTeamA.value?.plus(score))
        number.firstName = "this is firstName " + Random.nextInt(100)
    }

    fun increaseScoreTeamB(score: Int) {
        scoreTeamB.postValue(scoreTeamB.value?.plus(score))
        number.lastName = "this is lastName " + Random.nextInt(100)
    }

    init {
        scoreTeamA.value = 0
        scoreTeamB.value = 0
        number = Number()
    }
}