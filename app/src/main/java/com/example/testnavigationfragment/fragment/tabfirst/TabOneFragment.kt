package com.example.testnavigationfragment.fragment.tabfirst

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.testnavigationfragment.R
import com.example.testnavigationfragment.viewmodel.CountNumberViewModel
import kotlinx.android.synthetic.main.fragment_tab_one.*

class TabOneFragment : Fragment() {
    companion object {
        private const val ARG_PARAM1 = "param1"
        private const val ARG_PARAM2 = "param2"

        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            TabOneFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    private var param1: String? = null
    private var param2: String? = null

    private lateinit var mViewModel: CountNumberViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_tab_one, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        registerLiveDataListener()
    }

    private fun initView() {
        mViewModel =
            activity?.let { ViewModelProviders.of(it).get(CountNumberViewModel::class.java) }!!

        btnCount1.setOnClickListener {
            mViewModel.increaseScoreTeamA(1)
        }
        btnCount2.setOnClickListener { mViewModel.increaseScoreTeamA(2) }
        btnCount3.setOnClickListener { mViewModel.increaseScoreTeamA(3) }
        btnCount4.setOnClickListener { mViewModel.increaseScoreTeamB(1) }
        btnCount5.setOnClickListener { mViewModel.increaseScoreTeamB(2) }
        btnCount6.setOnClickListener { mViewModel.increaseScoreTeamB(3) }
    }

    private fun registerLiveDataListener() {
        mViewModel.scoreTeamA.observe(this, Observer {
            tvCountA.text = it.toString()
        })

        mViewModel.scoreTeamB.observe(this, Observer {
            tvCountB.text = it.toString()
        })
    }
}
