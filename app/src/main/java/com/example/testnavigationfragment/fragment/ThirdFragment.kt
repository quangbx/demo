package com.example.testnavigationfragment.fragment

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import android.os.Bundle
import android.os.SystemClock
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.fragment.app.Fragment
import com.example.testnavigationfragment.R
import com.example.testnavigationfragment.utils.Const
import kotlinx.android.synthetic.main.fragment_third.*
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL

class ThirdFragment : Fragment() {
    companion object {
        private const val ARG_PARAM1 = "param1"
        private const val ARG_PARAM2 = "param2"

        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            ThirdFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    private var param1: String? = null
    private var param2: String? = null
    private lateinit var myAsyncTask: MyAsyncTask

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_third, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    private fun initView() {
        runAsynTask.setOnClickListener {
            myAsyncTask = MyAsyncTask(this)
            if (!progressBar.isShown) myAsyncTask.execute(Const.url)
        }
    }

    override fun onResume() {
        super.onResume()
        if (::myAsyncTask.isInitialized && myAsyncTask.status != AsyncTask.Status.FINISHED) {
            myAsyncTask.cancel(true)
            progressBar.visibility = View.INVISIBLE
        }
    }

    class MyAsyncTask(var fragment: ThirdFragment?) : AsyncTask<String, Int, Bitmap>() {

        override fun onPreExecute() {
            fragment?.apply {
                progressBar?.visibility = ProgressBar.VISIBLE
                imageView.visibility = ProgressBar.INVISIBLE
            }
        }

        override fun doInBackground(vararg params: String): Bitmap? {
            for (i in 0..20) {
                SystemClock.sleep(100)
                publishProgress(i)
            }
            return downloadBitmap(params[0])
        }

        override fun onProgressUpdate(vararg values: Int?) {
            super.onProgressUpdate(*values)
            fragment?.apply {
                progressBar.progress = values[0]!!
                values.forEach {
                    second.text = it.toString()
                }
            }
        }

        override fun onPostExecute(result: Bitmap?) {
            fragment?.apply {
                imageView.setImageBitmap(result)
                imageView.visibility = ProgressBar.VISIBLE
                progressBar.visibility = ProgressBar.INVISIBLE
            }
        }

        private fun downloadBitmap(url: String): Bitmap? {
            var urlConnection: HttpURLConnection? = null
            try {
                val uri = URL(url)
                urlConnection = uri.openConnection() as HttpURLConnection
                val inputStream: InputStream = urlConnection.inputStream
                return BitmapFactory.decodeStream(inputStream)
            } catch (e: Exception) {
                Log.d("URLCONNECTIONERROR", e.toString())
                urlConnection?.disconnect()
            } finally {
                urlConnection?.disconnect()
            }
            return null
        }
    }
}
