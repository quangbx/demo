package com.example.testnavigationfragment.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.size
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentPagerAdapter
import com.example.testnavigationfragment.R
import com.example.testnavigationfragment.adapter.FirstPagerAdapter
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.fragment_first.*

class FirstFragment : Fragment() {
    companion object {
        private const val ARG_PARAM1 = "title"
        private const val ARG_PARAM2 = "page"

        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            FirstFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    // Store instance variables
    private var title: String? = null
    private var page: String? = null
    private lateinit var adapterViewPager: FragmentPagerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            title = it.getString(ARG_PARAM1)
            page = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_first, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    private fun initView() {
        tabLayout.addTab(tabLayout.newTab().setText(R.string.TAB_1), 0)
        tabLayout.addTab(tabLayout.newTab().setText(R.string.TAB_2), 1)
        tabLayout.addTab(tabLayout.newTab().setText(R.string.TAB_3), 2)
        tabLayout.tabMode = TabLayout.MODE_FIXED

        adapterViewPager =
            this.context?.let { FirstPagerAdapter(childFragmentManager, tabLayout.size, it) }!!
        vpPager?.adapter = adapterViewPager
    }
}