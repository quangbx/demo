package com.example.testnavigationfragment

import android.annotation.SuppressLint
import android.content.res.Configuration
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.core.view.get
import androidx.core.view.size
import androidx.fragment.app.FragmentPagerAdapter
import com.example.testnavigationfragment.adapter.TabPagerAdapter
import com.example.testnavigationfragment.widget.NonSwipeAbleViewPager
import com.google.android.material.navigation.NavigationView
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.activity_main.*
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*

class MainActivity : AppCompatActivity() {

    companion object {
        const val TAB_FIRST = 0
        const val TAB_TWO = 1
        const val TAB_THIRD = 2
    }

    private lateinit var toolbar: Toolbar

    // Make sure to be using androidx.appcompat.app.ActionBarDrawerToggle version.
    private lateinit var drawerToggle: ActionBarDrawerToggle

    private lateinit var adapterViewPager: FragmentPagerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Set a Toolbar to replace the ActionBar.
        toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        // This will display an Up icon (<-), we will replace it with hamburger later
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        // Find our drawer view
        drawerToggle = setupDrawerToggle()

        // Setup drawer view
        setupDrawerContent(nvDrawer)

        // Setup toggle to display hamburger icon with nice animation
        drawerToggle.isDrawerIndicatorEnabled = true
        drawerToggle.syncState()

        setUpTabBottom()
        test()
    }

    private fun setUpTabBottom() {
        val viewPager = findViewById<NonSwipeAbleViewPager>(R.id.view_page_head)

        tabLayout.addTab(tabLayout.newTab().setText(R.string.FIRST), TAB_FIRST)
        tabLayout.addTab(tabLayout.newTab().setText(R.string.SECOND), TAB_TWO)
        tabLayout.addTab(tabLayout.newTab().setText(R.string.THIRD), TAB_THIRD)
        tabLayout.tabMode = TabLayout.MODE_FIXED
        adapterViewPager = TabPagerAdapter(this.supportFragmentManager, tabLayout.size, this)
        viewPager.adapter = adapterViewPager

        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                toolbar.title = tab.text
                nvDrawer.menu[tab.position].isChecked = true
            }

            override fun onTabReselected(tab: TabLayout.Tab) {
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // The action bar home/up action should open or close the drawer.
        when (item.itemId) {
            R.id.home -> {
                mDrawer.openDrawer(GravityCompat.START)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setupDrawerContent(navigationView: NavigationView) {
        navigationView.setNavigationItemSelectedListener { menuItem ->
            selectDrawerItem(menuItem)
            true
        }
    }

    private fun selectDrawerItem(menuItem: MenuItem) {
        when (menuItem.itemId) {
            R.id.nav_first_fragment -> tabLayout.getTabAt(TAB_FIRST)?.select()
            R.id.nav_second_fragment -> tabLayout.getTabAt(TAB_TWO)?.select()
            R.id.nav_third_fragment -> tabLayout.getTabAt(TAB_THIRD)?.select()
            else -> return
        }

        // Highlight the selected item has been done by NavigationView
        menuItem.isChecked = true
        // Set action bar title
        title = menuItem.title
        // Close the navigation drawer
        mDrawer.closeDrawers()
    }

    private fun setupDrawerToggle(): ActionBarDrawerToggle {
        // NOTE: Make sure you pass in a valid toolbar reference.  ActionBarDrawToggle() does not require it
        // and will not render the hamburger icon without it.
        return ActionBarDrawerToggle(
            this,
            mDrawer,
            toolbar,
            R.string.drawer_open,
            R.string.drawer_close
        )
    }

    // `onPostCreate` called when activity start-up is complete after `onStart()`
    // NOTE 1: Make sure to override the method with only a single `Bundle` argument
    // Note 2: Make sure you implement the correct `onPostCreate(Bundle savedInstanceState)` method.
    // There are 2 signatures and only `onPostCreate(Bundle state)` shows the hamburger icon.
    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        // Sync the toggle state after onRestoreInstanceState has occurred.
        drawerToggle.syncState()
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        // Pass any configuration change to the drawer toggles
        drawerToggle.onConfigurationChanged(newConfig);
    }

    @SuppressLint("NewApi")
    private fun test() {
        val sdf = SimpleDateFormat("dd/M/yyyy hh:mm:ss")

        val string = "2017-07-25"
        val date = LocalDate.parse(string, DateTimeFormatter.ISO_DATE)

        println("Quangbx: ${date.month}")

        val startOfToday = getStartOfToday()
        System.out.println("Quangbx: startOfToday is " + sdf.format(startOfToday))

        val startOfTomorrow = getNextDayFromDay(startOfToday)
        System.out.println("Quangbx: startOfTomorrow is " + sdf.format(startOfTomorrow))

        val startOfTwoNextMonth = getNextTwoMonthFromDay(startOfToday)
        System.out.println("Quangbx: startOfTwoNextMonth is " + sdf.format(startOfTwoNextMonth))
    }

    private fun getStartOfToday(): Date {
        val calendar = Calendar.getInstance()
        calendar.time = Date()
        return calendar.time
    }

    private fun getNextDayFromDay(date: Date): Date {
        val calendar = Calendar.getInstance()
        calendar.time = date
        calendar.add(Calendar.DATE, 1)
        return calendar.time
    }

    private fun getNextTwoMonthFromDay(date: Date): Date {
        val calendar = Calendar.getInstance()
        calendar.time = date
        calendar.add(Calendar.MONTH, 2)
        return calendar.time
    }
}