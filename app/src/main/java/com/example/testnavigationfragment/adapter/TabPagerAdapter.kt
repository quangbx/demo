package com.example.testnavigationfragment.adapter

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.example.testnavigationfragment.R
import com.example.testnavigationfragment.fragment.FirstFragment
import com.example.testnavigationfragment.fragment.SecondFragment
import com.example.testnavigationfragment.fragment.ThirdFragment

class TabPagerAdapter(fm: FragmentManager, behavior: Int, private val context: Context) :
    FragmentPagerAdapter(fm, behavior) {

    companion object {
        const val NUM_ITEMS = 3
    }

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> FirstFragment.newInstance(
                context.getString(R.string.FIRST),
                context.getString(R.string.FIRST)
            )
            1 -> SecondFragment.newInstance(
                context.getString(R.string.SECOND),
                context.getString(R.string.SECOND)
            )
            else -> ThirdFragment.newInstance(
                context.getString(R.string.THIRD),
                context.getString(R.string.THIRD)
            )
        }
    }

    override fun getCount(): Int {
        return NUM_ITEMS
    }

    // Returns the page title for the top indicator
    override fun getPageTitle(position: Int): CharSequence? {
        return when (position) {
            0 -> context.getString(R.string.FIRST)
            1 -> context.getString(R.string.SECOND)
            else -> context.getString(R.string.THIRD)
        }
    }
}