package com.example.testnavigationfragment.adapter

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.example.testnavigationfragment.R
import com.example.testnavigationfragment.fragment.tabfirst.TabOneFragment
import com.example.testnavigationfragment.fragment.tabfirst.TabThreeFragment
import com.example.testnavigationfragment.fragment.tabfirst.TabTwoFragment

class FirstPagerAdapter(fm: FragmentManager, behavior: Int, private val context: Context) :
    FragmentPagerAdapter(fm, behavior) {
    companion object {
        const val NUM_ITEMS = 3
    }

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> TabOneFragment.newInstance(
                context.getString(R.string.TAB_1),
                context.getString(R.string.TAB_1)
            )
            1 -> TabTwoFragment.newInstance(
                context.getString(R.string.TAB_2),
                context.getString(R.string.TAB_2)
            )
            else -> TabThreeFragment.newInstance(
                context.getString(R.string.TAB_3),
                context.getString(R.string.TAB_3)
            )
        }

    }

    override fun getCount(): Int {
        return NUM_ITEMS
    }

    // Returns the page title for the top indicator
    override fun getPageTitle(position: Int): CharSequence? {
        return when (position) {
            0 -> context.getString(R.string.TAB_1)
            1 -> context.getString(R.string.TAB_2)
            else -> context.getString(R.string.TAB_3)
        }
    }
}
